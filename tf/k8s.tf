resource "google_service_account" "default" {
  account_id   = "bieber-gcp" + local.postifx
  display_name = "bieber-gcp" + local.postifx
}

resource "google_container_cluster" "primary" {
  name     = "my-gke-cluster"+ local.postifx
  location = "asia-northeast3-a"

  remove_default_node_pool = true
  initial_node_count       = 1
  network = google_compute_network.vpc.name
  subnetwork = google_compute_subnetwork.subnet.name
}

resource "google_container_node_pool" "primary_preemptible_nodes" {
  name       = "my-node-pool" + local.postifx
  location   = "asia-northeast3-a"
  cluster    = google_container_cluster.primary.name
  node_count = 2

  node_config {
    preemptible  = true
    machine_type = "e2-medium"
    service_account = google_service_account.default.email
    oauth_scopes    = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
  }
}