variable "build_env" {
  type        = string
  description = "dev or stage"
  validation {
    condition     = var.build_env == "dev" || var.build_env == "stage"
    error_message = "build_env must be one of dev stage prod"
  }
}

locals {
  postfix = "-"+var.build_env
  prefix = var.build_env+"-"
  projectId = var.build_env == "dev" ? "kubernetes-316400" : "kubernetes-stage"
}