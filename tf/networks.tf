resource "google_compute_network" "vpc" {
  name = "vpc-gke-demo"
  project = "kubernetes-316400"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "subnet" {
  name          = "test-subnetwork"
  ip_cidr_range = "10.0.0.0/16"
  region        = "asia-northeast3"
  network       = google_compute_network.vpc.id
}
