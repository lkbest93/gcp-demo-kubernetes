terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.5.0"
    }
  }
  backend "gcs" {
    bucket = "tfstate-bieber-kube" + local.postfix
    prefix = "terraform/state"
  }
}
provider "google" {
  project = local.projectId
  region  = "asia-northeast3"
  zone    = "asia-northeast3-a"
}
